module caldavwarrior;
import std.stdio : writeln;
import std.json : JSONValue;
import std.typecons;

import caldavwarrior.file;

alias Pair = Tuple!(string, "first", string, "second");

private auto iCalSupportedKeys = ["DTSTART","CREATED","LAST-MODIFIED",
    "SUMMARY","PRIORITY","CATEGORIES","STATUS","COMPLETED","DUE"];

private auto twSupportedKeys = ["scheduled","entry","modified",
    "description","priority","tags","status","end","due"];

string iCalPath(string project, string uid, Config config) {
    import std.path : buildPath;
    return buildPath(config.caldavPath, project, uid ~ ".vcf");
}

/+
    BEGIN:VCALENDAR
    VERSION:2.0
    PRODID:-//Nextcloud Tasks vundefined
    BEGIN:VTODO
    CREATED:20180930T115836
    DTSTAMP:20180930T141702
    LAST-MODIFIED:20180930T141702
    UID:c140cf5b2c
    SUMMARY:Nextcloud test task
    PRIORITY:0
    PERCENT-COMPLETE:0
    X-OC-HIDESUBTASKS:0
    DESCRIPTION:Adding some notes to my todo
    CATEGORIES:category1,tag2
    END:VTODO
    END:VCALENDAR⏎
+/
auto parseICal(in string cal, bool supported = true) {
    // supported only keeps those values
    // unsupported keeps the rest
    import std.algorithm : canFind, map, filter;
    import std.regex : match, regex, splitter;
    return cal.splitter(regex("\n")).
        map!(s => s.match(regex("([A-Z-]+):(.+)$"))).
        map!(m => Pair(m.captures[1], m.captures[2])).
        filter!(t => iCalSupportedKeys.canFind(t.first) == supported);
}

unittest {
    import std.range : walkLength;
    auto parsed = parseICal("BEGIN:VTODO
        CREATED:20180930T115836");
    assert(parsed.walkLength == 1);
    assert(parsed.front.first == "CREATED");
    assert(parsed.front.second == "20180930T115836");
}

auto toICal(in JSONValue taskJson) {
    import std.array : appender;
    auto ical = appender!(Pair[]);
    if ("entry" in taskJson)
        ical.put(Pair("CREATED", taskJson["entry"].str));
    if ("modified" in taskJson)
        ical.put(Pair("LAST-MODIFIED", taskJson["modified"].str));
    if ("description" in taskJson)
        ical.put(Pair("SUMMARY", taskJson["description"].str));
    if ("tags" in taskJson) {
        import std.algorithm : joiner, map;
        import std.conv : to;
        ical.put(Pair("CATEGORIES", taskJson["tags"].array.map!(s => s.str).joiner(",").to!string));
    }
    if ("status" in taskJson) {
        if (taskJson["status"].str == "completed") {
            ical.put(Pair("STATUS", "COMPLETED"));
            ical.put(Pair("COMPLETED", taskJson["end"].str));
        } else if (taskJson["status"].str == "pending") {
            ical.put(Pair("STATUS","NEEDS-ACTION"));
        }
    }
    if ("scheduled" in taskJson) {
        ical.put(Pair("DTSTART", taskJson["scheduled"].str));
    }
    if ("due" in taskJson) {
        ical.put(Pair("DUE", taskJson["due"].str));
    }
    if ("priority" in taskJson) {
        import std.conv : to;
        auto pr = taskJson["priority"].str;
        int value = 4;
        if (pr == "L")
            value = 8;
        else if (pr == "M")
            value = 6;
        else if (pr == "H")
            value = 2;
        ical.put(Pair("PRIORITY", value.to!string));
    }
    return ical.data;
}

unittest {
    import std.array : front;
    import std.json : parseJSON;
    auto rst = toICal(parseJSON("{\"description\":\"bla\"}"));
    assert(rst.front.first == "SUMMARY");
    assert(rst.front.second == "bla");
    rst = toICal(parseJSON(q{{"tags":["bla","bla1"]}}));
    assert(rst.front.first == "CATEGORIES");
    assert(rst.front.second == "bla,bla1");
}

auto baseICal(string dtstamp, string uid) {
    // We probably should include an UID and DTSTAMP here
    return [
        Pair("BEGIN", "VCALENDAR"),
        Pair("VERSION", "2.0"),
        Pair("PRODID", "-//caldavwarrior"),
        Pair("BEGIN","VTODO"),
        Pair("DTSTAMP", dtstamp),
        Pair("UID", uid),
        Pair("END","VTODO"),
        Pair("END","VCALENDAR")
    ];
}

auto mergeICal(R, T)(R orig, T update) {
    // Insert updated values after UID
    import std.algorithm.searching : findSplitAfter;
    import std.array : array;
    import std.range : chain;
    auto sp = orig.findSplitAfter!((a,b) => a.first == b)(["UID"]);
    return sp[0].chain(update, sp[1]);
}

auto mergeICal(R : string, T)(R orig, T update) {
   return mergeICal(parseICal(orig, false), update);
}

unittest {
    import std.range : walkLength;
    auto base = baseICal("a", "b");
    auto merged = mergeICal(base, [Pair("SUMMARY", "Bla")]);
    assert(merged.walkLength == base.walkLength + 1);
}

void writeICal(T)(T ical, string projectPath) {
    import std.stdio : toFile;
    import std.file : mkdir, exists;
    import std.path : buildPath;
    import std.algorithm.searching : find;
    import std.algorithm : map, joiner, filter;
    import std.conv : to;
    import std.range : empty;
    if (!exists(projectPath)) {
        projectPath.mkdir;
    }
    string uid = ical.find!((a,b) => a.first == b)("UID").front.second;
    ical.
        filter!(l => !l.first.empty && !l.second.empty).
        map!(l => l.first ~ ":" ~ l.second).
        joiner("\n").to!string.
        toFile(buildPath(projectPath, uid ~ ".vcf"));
}

/+
unittest {
    import std.range : walkLength;
    auto base = baseICal("a", "b");
    auto merged = mergeICal(base, [Pair("SUMMARY", "Bla")]);
    writeICal(merged, "default");
}
+/

string getICalProject(JSONValue taskJson) {
    string proj = "default";
    if ("project" in taskJson)
        proj = taskJson["project"].str;
    return proj;
}

auto parseSyncOutput(in string text, string vdirLocalStorage) {
    /+
        Changes (note updating):
        Copying (updating) item 0ec194f5-4146-4830-ba50-5943c9a4038d to cal_local/default
        Copying (updating) item 4b20347b-650f-4ef0-b861-b3cab43a18e5 to cal_local/default
        Addition (note uploading):
        Copying (uploading) item lds9579ais to cal_local/personal
        Deleting
        Deleting item lds9579ais from cal_local/personal
    +/
    import std.algorithm : splitter, filter, map;
    import std.regex : match, regex;
    return text.splitter("\n").
        filter!(a => a.match(regex(vdirLocalStorage ~ "/"))).
        map!(a => a.match(regex("(Deleting|up[a-z]+).* item (.*) .* " ~
            vdirLocalStorage ~ "/(.*)"))).
        map!(a => Tuple!(string, "action", string, "uid", string, "project")(a.front[1], a.front[2], a.front[3]));
}

unittest {
    import std.range : walkLength;
    string input = "Changes (note updating):
        Copying (updating) item 0ec194f5-4146-4830-ba50-5943c9a4038d to cal_local/default
        Copying (updating) item 4b20347b-650f-4ef0-b861-b3cab43a18e5 to cal_local/default
        Addition (note uploading):
        Copying (uploading) item lds9579ais to cal_local/personal
        Deleting
        Deleting item lds9579ais from cal_local/personal";
    assert(input.parseSyncOutput("cal_local").walkLength == 4);

}

auto tempFileName() {
    import std.ascii : letters;
    import std.conv : to;
    import std.file : tempDir;
    import std.path : buildPath;
    import std.random : randomSample;
    import std.utf : byCodeUnit;

    // random id with 20 letters
    auto id = letters.byCodeUnit.randomSample(20).to!string;
    return tempDir.buildPath(id);
}

struct Export {
    bool success = false;
    JSONValue task;
}

auto exportTaskFromShell(string uid) {
    import std.process : executeShell;
    import std.range : empty, front;
    import std.json : parseJSON;
    auto sh = executeShell("task export rc.json.array=off " ~ uid);
    Export result;
    if (sh.output.empty || sh.output.front != '{') {
        return result;
    }
    import std.algorithm : splitter;
    result.task = sh.output.splitter("\n").front.parseJSON;
    result.success = true;
    return result;
}

string importTaskFromShell(JSONValue taskJson, bool hooks = true) {
    import std.stdio : toFile;
    import std.file : remove;
    import std.process : executeShell;
    auto fn = tempFileName();
    taskJson.toString.toFile(fn);
    scope(exit) fn.remove;

    if (!hooks)
        fn = "rc.hooks=off " ~ fn;
    auto sh = executeShell("task import " ~ fn);
    import std.regex : matchFirst, regex;
    return sh.output.
        matchFirst(regex("[A-z0-9]+-[A-z0-9]+-[A-z0-9]+-[A-z0-9]+-[A-z0-9]+")).front;
}

auto extractSupported(JSONValue taskJson, bool invert = false) {
    import std.algorithm : canFind;
    JSONValue root;
    foreach(key, value; taskJson.object) {
        if (twSupportedKeys.canFind(key) != invert) {
            root[key] = value;
        }
    }
    return root;
}

unittest {
    import std.json : parseJSON;
    assert(extractSupported(q{{"bla":"blaat","due":"a"}}.parseJSON).toString ==
        q{{"due":"a"}});
    assert(extractSupported(q{{"bla":"blaat","due":"a"}}.parseJSON, true).toString ==
        q{{"bla":"blaat"}});
}

auto toTW(T : string)(T ical, string project) {
    return toTW(ical.parseICal, project);
}
//private auto iCalSupportedKeys = ["CREATED","LAST-MODIFIED","SUMMARY",
//    "PRIORITY","CATEGORIES","STATUS","COMPLETED","DUE"];
//private auto twSupportedKeys = ["entry","modified","description",
//    "priority","tags","status","end","due"];
auto toTW(T)(T ical, string project) {
    JSONValue root;
    foreach(pair; ical) {
        if (pair.first == "CREATED")
            root["entry"] = pair.second;
        else if (pair.first == "LAST-MODIFIED")
            root["modified"] = pair.second;
        else if (pair.first == "SUMMARY")
            root["description"] = pair.second;
        else if (pair.first == "PRIORITY") {
            import std.conv : to;
            auto value = pair.second.to!int;
            if (value == 0) // Bit of a cheat, but value of 0 is treated like unset priority
                continue;
            string priority = "L";
            if (value <= 2)
                priority = "H";
            else if (value <= 4)
                continue;
            else if (value <= 6)
                priority = "M";
            root["priority"] = priority;
        } else if (pair.first == "CATEGORIES") {
            import std.algorithm : splitter;
            import std.array : array;
            root["tags"] = pair.second.splitter(",").array;
        } else if (pair.first == "STATUS") {
            if (pair.second == "COMPLETED")
                root["status"] = "completed";
            else if (pair.second == "NEEDS-ACTION")
                root["status"] = "pending";
        } else if (pair.first == "COMPLETED")
            root["end"] = pair.second;
        else if (pair.first == "DUE")
            root["due"] = pair.second;
    }
    if (project != "default")
        root["project"] = project;
    return root;
}

unittest {
    assert("CATEGORIES:bla,bla1\n".toTW("default").toString ==
        q{{"tags":["bla","bla1"]}});
}

auto mergeTask(JSONValue base, JSONValue update) {
    foreach(key, value; update.object) {
        base[key] = value;
    }
    return base;
}
