module caldavwarrior.file;

struct UIDPair {
    string tw;
    string ical;
}

auto loadUIDMap() {
    import standardpaths;
    import std.path : buildPath;
    import std.file : exists, readText;
    import std.json : parseJSON;
    import painlessjson : fromJSON;
    string dataFile = writablePath(StandardPath.data, "caldavwarrior", FolderFlag.create).
        buildPath("uidmap.json");
    if (!exists(dataFile)) {
        UIDPair[] uidMap;
        writeUIDMap(uidMap);
        return uidMap;
    }
    return dataFile.readText.parseJSON.fromJSON!(UIDPair[]);
}

void writeUIDMap(UIDPair[] uidMap) {
    import standardpaths;
    import std.path : buildPath;
    import painlessjson : toJSON;
    import std.stdio : toFile;
    string dataFile = writablePath(StandardPath.data, "caldavwarrior", FolderFlag.create).
        buildPath("uidmap.json");
    uidMap.toJSON.toString.toFile(dataFile);
}

struct Config {
    string caldavPath;
    string vdirLocalStorage = "cal_local";
}

auto loadConfig() {
    import standardpaths;
    import std.path : buildPath;
    import std.file : exists, readText;
    import std.json : parseJSON;
    import painlessjson : fromJSON;
    string dataFile = writablePath(StandardPath.config, "caldavwarrior", FolderFlag.create).
        buildPath("config.json");
    if (!exists(dataFile)) {
        Config config;
        config.caldavPath = writablePath(StandardPath.config, 
            buildPath("vdirsyncer", "calendar"), FolderFlag.none);
        writeConfig(config);
        return config;
    }
    return dataFile.readText.parseJSON.fromJSON!(Config);
}

void writeConfig(Config config) {
    import standardpaths;
    import std.path : buildPath;
    import painlessjson : toJSON;
    import std.stdio : toFile;
    string dataFile = writablePath(StandardPath.config, "caldavwarrior", FolderFlag.create).
        buildPath("config.json");
    config.toJSON.toPrettyString.toFile(dataFile);
}

