import std.stdio;
import std.typecons;
import std.file;
import std.json;
import std.path;

import caldavwarrior;
import caldavwarrior.file;

/+
Need uuid uid map
When modified caldav task, need to task export rc.json.array=off, then add/modify the new values and then cal task import
+/

void main(string[] args)
{
    auto command = "status";
    if (args.length >= 2) {
        command = args[1];
    } 
    if (args.length == 1 || command[0..4] == "api:") {
        import std.regex;
        auto name = args[0].baseName;
        if (name[0..3] == "on-") {
            command = name.matchAll(regex("on-([A-z]+)")).front[1];
        }
    }

    auto uidMap = loadUIDMap();
    auto config = loadConfig();
    if (command == "add") {
        import std.algorithm : find;
        import std.range : front, empty;
        import std.path : buildPath;
        auto newIn = readln();
        write(newIn);
        auto taskJson = newIn.parseJSON();
        auto twUID = taskJson["uuid"].str;
        auto iCalUID = uidMap.find!((a,b) => a.tw == b)(twUID);
        if (iCalUID.empty) {
            uidMap = UIDPair(twUID, twUID) ~ uidMap;
            writeUIDMap(uidMap);
            iCalUID = uidMap.find!((a,b) => a.tw == b)(twUID);
        }
        baseICal(taskJson["entry"].str, iCalUID.front.ical).
            mergeICal(toICal(taskJson)).
            writeICal(buildPath(config.caldavPath, taskJson.getICalProject));
    } else if (command == "modify") {
        auto oldIn = readln();
        auto newIn = readln();
        write(newIn);

        // Delete old file
        auto oldTaskJson = oldIn.parseJSON();
        auto taskJson = newIn.parseJSON();

        import std.algorithm : find;
        import std.range : empty, front;
        auto twUID = oldTaskJson["uuid"].str;
        auto iCalUID = uidMap.find!((a,b) => a.tw == b)(twUID);
        string oldFile;
        if (!iCalUID.empty) {
            oldFile = iCalPath(oldTaskJson.getICalProject, iCalUID.front.ical, config);
        } else {
            oldFile = iCalPath(oldTaskJson.getICalProject, twUID, config);
            uidMap = UIDPair(twUID, twUID) ~ uidMap;
            writeUIDMap(uidMap);
            iCalUID = uidMap.find!((a,b) => a.tw == b)(twUID);
        }
        if (std.file.exists(oldFile)) {
            import std.file : readText;
            auto text = oldFile.readText;
            oldFile.remove;
            if (!("status" in taskJson && 
                    taskJson["status"].str == "deleted")) {
                text.parseICal(false).
                    mergeICal(taskJson.toICal).
                    writeICal(buildPath(config.caldavPath, taskJson.getICalProject));
            } else { // deleted
                import std.algorithm : remove;
                uidMap = uidMap.remove!(a => a.tw == twUID);
                writeUIDMap(uidMap);
            }
        } else {
            if (!("status" in taskJson && 
                    taskJson["status"].str == "deleted")) {
                baseICal(taskJson["entry"].str, iCalUID.front.ical).
                    mergeICal(toICal(taskJson)).
                    writeICal(buildPath(config.caldavPath, taskJson.getICalProject));
            }
        }
    } else if (command == "sync") {
        import std.process : executeShell;
        auto disc = executeShell("yes | vdirsyncer discover");
        disc.output.writeln;
        auto sync = executeShell("vdirsyncer sync");
        auto actions = sync.output.parseSyncOutput(config.vdirLocalStorage);
        sync.output.writeln;
        
        import std.range : front;
        foreach(action; actions) {
            if (action.action == "Deleting") {
                import std.algorithm : find, remove;
                import std.range : empty, front;
                auto twUID = action.uid;
                auto twUIDNeedle = uidMap.find!((a,b) => a.ical == b)(action.uid);
                if (!twUIDNeedle.empty) {
                    twUID = twUIDNeedle.front.tw;
                    uidMap = uidMap.remove!(a => a.tw == twUID);
                    writeUIDMap(uidMap);
                }
                auto sh = exportTaskFromShell(twUID);
                if (!sh.success) {
                    ("Delete: could not find " ~ action.uid).writeln;
                    continue;
                }
                auto taskJson = sh.task;
                taskJson["status"] = "deleted";
                importTaskFromShell(taskJson);
            } else if (action.action == "updating") {
                import std.file : exists, readText;
                // load new caldav file
                auto oldFile = iCalPath(action.project, action.uid, config);
                if (!exists(oldFile)) {
                    ("Updating: could not find " ~ oldFile).writeln;
                    continue;
                }
                auto ical = oldFile.readText;

                import std.algorithm : find;
                import std.range : empty, front;
                auto twUID = action.uid;
                auto twUIDNeedle = uidMap.find!((a,b) => a.ical == b)(action.uid);
                if (!twUIDNeedle.empty) 
                    twUID = twUIDNeedle.front.tw;
                // task export the current version in tw
                auto sh = exportTaskFromShell(twUID);
                if (!sh.success) {
                    action.action = "uploading";
                } else {
                    auto base = extractSupported(sh.task, true);
                    //auto newTask = mergeTask(base, toTW(caldav file, action.project));
                    auto newTask = mergeTask(base, toTW(ical, action.project));
                    // Import new version into task (as above)
                    importTaskFromShell(newTask);
                }
            }
            // This is not else if, because the updating action might call this
            if (action.action == "uploading") {
                import std.uuid : randomUUID;
                auto oldFile = iCalPath(action.project, action.uid, config);
                if (!exists(oldFile)) {
                    ("UpLoading: could not find " ~ oldFile).writeln;
                    continue;
                }
                auto ical = oldFile.readText.parseICal;
                string twUID = randomUUID.toString;
                auto newTask = mergeTask(("{\"uuid\":\"" ~ twUID ~ "\"}").parseJSON, 
                    toTW(ical, action.project));
                uidMap = UIDPair(twUID, action.uid) ~ uidMap;
                writeUIDMap(uidMap);
                importTaskFromShell(newTask);
            }
        }
        import std.range : walkLength;
        if (actions.walkLength > 0)
           sync = executeShell("vdirsyncer sync");
    }
}
