# caldavwarrior

Sync taskwarrior tasks with caldav tasks, such as [nextcloud tasks](https://apps.nextcloud.com/apps/tasks). It relies on [vdirsyncer](http://vdirsyncer.pimutils.org/en/stable/) to sync the caldav server to your local machine and then caldavwarrior converts between taskwarrior and caldav tasks. Currently it has only been tested with nextcloud tasks, but it should work with other caldav servers (google tasks), as long as they are supported by vdirsyncer.

# Installation

Caldavwarrior is written in D and needs a working D compiler. For instructions on how to install the D compiler and dub see [here](https://dlang.org/download.html)

After installation it is a simple matter of cloning the repository and running dub to compile `caldavwarrior`

```shell
git clone https://gitlab.com/BlackEdder/caldavwarrior.git
cd caldavwarrior
dub
```

This will result in a compiled binary `caldavwarrior` that you can put anywhere in your path. To integrate it with taskwarrior you have to install the hooks. On linux the easiest way to do this is by linking to the binary:

```shell
ln -s /path/to/caldavwarrior ~/.task/hooks/on-modify.caldavwarrior
ln -s /path/to/caldavwarrior ~/.task/hooks/on-add.caldavwarrior
```

## Configuration

If you use the provided vdirsyncer config example then the default configuration should work. If not then you need to edit the caldavwarrior config file (default location `~/.config/caldavwarrior/config.json`). The settings in the config file need to correspond to the `[storage cal_local]` section in your vdirsyncer config. See [below](#vdirsyncer-setup).


```json
{
    "caldavPath": "/path/to/vdirsync/calendar",
    "vdirLocalStorage": "cal_local"
}
```

## Vdirsyncer setup

For full details on how to setup and configure vdirsyncer visit their [website](http://vdirsyncer.pimutils.org/en/stable/). The main part you need is a config file (`~/.config/vdirsyncer/config`). As a reference a working config file for nextcloud would look as follows. Note you need to provide the url, username and password of your nextcloud server.

```ini
[general]
status_path = "~/.config/vdirsyncer/status/"

[pair cal]
a = "cal_local"
b = "cal_remote"
collections = ["from a", "from b"]

[storage cal_local]
type = "filesystem"
path = "~/.config/vdirsyncer/calendar/"
fileext = ".vcf"

[storage cal_remote]
type = "caldav"
url = ""
username = ""
password = ""
```

# Usage

Whenever you create and modify tasks with taskwarrior they will automatically be added to your local caldav storage. To sync them to your caldav server you need to run:

```shell
caldavwarrior sync
```

This will use `vdirsyncer` to sync with the caldav server and then import any new or changed tasks to your taskwarrior tasks.

Currently caldavwarrior will only convert fields/features that are directly convertible. Features that only exist in one of the applications (i.e. wait in taskwarrior) will be ignored, i.e. wait will work for taskwarrior as normal, but have no effect on the caldav side of things.
